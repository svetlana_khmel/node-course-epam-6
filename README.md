# HOMEWORK 6 #


### SQL DATABASES. ORM ###

### Tasks ###

```
Install postgresql (any way acceptable but the usage of docker container is preferred).
Install sequelize package.
Create database and models directory.
Setup sequelize to work with your project structure.
Use sequelize cli tool to create users and products tables.
Create migrations for users.
Modify application to import product data from model file (the base could be taken from Homework 1) to the database.
Modify application to respond all routes from Homework 4 and return data from the database.

Evaluation Criteria
All required packages installed (tasks 1-2).
Database and related tables created; sequelize setup (tasks 3-5).
Database created with all required migrations (task 6).
The product data from file was imported to the database (task 7).
All routes respond with the data from database (task 8).

npm install -g sequelize-cli
```
### ARTICLES ###

- https://scotch.io/tutorials/getting-started-with-node-express-and-postgres-using-sequelize

- https://medium.com/@jzioria/how-i-fixed-my-sequelize-command-not-found-error-e3ec651b3abc

- https://www.youtube.com/watch?v=A8dErdDMqb0

- https://medium.com/@zbbergma/dockerizing-a-node-js-postgresql-app-backend-ac81750cf6df

- !!!! http://mherman.org/blog/2015/10/22/node-postgres-sequelize/

- http://docs.sequelizejs.com/manual/tutorial/migrations.html
- http://docs.sequelizejs.com/manual/installation/getting-started.html

How to import CSV file data into a PostgreSQL table?


CREATE TABLE zip_codes 
(ZIP char(5), LATITUDE double precision, LONGITUDE double precision, 
CITY varchar, STATE char(2), COUNTY varchar, ZIP_CLASS varchar);
Copy data from your CSV file to the table:

COPY zip_codes FROM '/path/to/csv/ZIP_CODES.txt' WITH (FORMAT csv);


How to import CSV file data into a PostgreSQL table?


=== COMMANDS USED ===



CREATE DATABASE demo_db1;
CREATE TABLE demo_t(something int);

\dt  
INSERT INTO demo_t (something) values (1);


** connect database
\c demo_db2 

=== How to exit from PostgreSQL command line utility: psql ===

Type \q and then press ENTER to quit psql.

quit()
quit
exit()
exit
q
q()
!q
^C
help
Alt + Tab
google.com
Quit PSQL
\q



=== DOCKER COMMANDS ===

* remove image

docker rm -f demo

**





1. Create a Postgres docker container
Docker run —name demo -e POSTGRES_PASSWORD=password1 -d postgres

2. Connect postgres and run some queries
Docker exec -it demo ps ql -U postgres


3. Automate - run scripts using docker cli
docker run --name demo -v "$PWD"/:/opt/demo/ -e POSTGRES_PASSWORD=password1 -d postgres

docker exec -it demo psql -U postgres -c “CREATE DATABASE demo_db2;”

docker exec -it demo sql -U postgres -f  /opt/demo/script_demo1.sql



How to get the name of the current database from within PostgreSQL?



How do I list all databases and tables using psql?

\list or \l: list all databases
\dt: list all tables in the current database
You will never see tables in other databases, these tables aren't visible. You have to connect to the correct database to see its tables (and other objects).

To switch databases:

\connect database_name




https://stackoverflow.com/questions/46608382/sequelize-deprecated-error-message
https://github.com/sequelize/sequelize/issues/8417


** Accidentally committed .idea directory files into git **
$ echo '.idea' >> .gitignore
$ git rm -r --cached .idea
$ git add .gitignore
$ git commit -m '(some message stating you added .idea to ignored entries)'
$ git push

