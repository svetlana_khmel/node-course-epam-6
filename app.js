const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const products = require('./routes/products');
const user = require('./routes/users');
const auth = require('./routes/auth');
const passportLocal = require('./routes/passportLocal');
const passportFacebook = require('./routes/passportFacebook');
const passport = require('passport');
const Sequelize = require('sequelize');

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(passport.initialize())
app.use(passport.session());

// const sequelize = new Sequelize('database', 'username', 'password', {
//   host: 'localhost',
//   dialect: 'mysql'|'sqlite'|'postgres'|'mssql',
//   operatorsAliases: false,
//
//   pool: {
//     max: 5,
//     min: 0,
//     acquire: 30000,
//     idle: 10000
//   },
//
//   // SQLite only
//   storage: 'path/to/database.sqlite'
// });

// Or you can simply use a connection uri
// const sequelize = new Sequelize('postgres://user:root:5432/demo_db1');
//
// sequelize
//   .authenticate()
//   .then(() => {
//     console.log('Connection has been established successfully.');
//   })
//   .catch(err => {
//     console.error('Unable to connect to the database:', err);
//   });

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.use('/api/products', products);
app.use('/api/users', user);
app.use('/auth', auth);
app.use('/local', passportLocal);
app.use('/auth/facebook', passportFacebook);

export default app;
