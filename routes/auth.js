const express = require('express');
const router = express.Router();
import jwt from 'jsonwebtoken';

router.post('/', (req, res) => {
  console.log('Auth... ', req.body);
  const { username, password } = req.body;
  if (username === 'test' || password === 'test') {
    let payload = {
      username
    };

    let token = jwt.sign(payload, 'secret');

    console.log('Auth...token ', token);
    res.send(token);
  } else {
    res.status(403).send({success: false, message: 'Bad user.'});
  }
});

module.exports = router;


// router.post('/auth', (req, res) => {
//   const { username, password } = req.body
//   if (username === 'test' || password === 'test') {
//     var token = jwt.sign({
//       username: username,
//       xsrfToken: crypto.createHash('md5').update(username).digest('hex')
//     }, 'jwtSecret', {
//       expiresIn: 60*60
//     });
//     res.status(200).json({
//       success: true,
//       message: 'OK',
//       token: token,
//       data: {
//         user: {
//           username,
//           email: `${username}@email.com`
//         }
//       }
//     });
//   } else {
//     res.status(400).json({
//       success: false,
//       message: 'Authentication failed'
//     })
//   }
// });
//
// module.exports = router;