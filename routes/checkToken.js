import jwt from 'jsonwebtoken';

const checkToken = (req, res, next) => {

  // check header or url parameters or post parameters for token
  let token = req.body.token || req.query.token || req.headers['x-access-token'];
  console.log('token ', token);
  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, 'secret', function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });

  }
};

module.exports = checkToken;




// const express = require('express');
// const router = express.Router();
// import jwt from 'jsonwebtoken';
//
// router.use(function(req, res, next) {
//
//   // check header or url parameters or post parameters for token
//   let token = req.body.token || req.query.token || req.headers['x-access-token'];
//
//   // decode token
//   if (token) {
//
//     // verifies secret and checks exp
//     jwt.verify(token, 'superSecret', function(err, decoded) {
//       if (err) {
//         return res.json({ success: false, message: 'Failed to authenticate token.' });
//       } else {
//         // if everything is good, save to request for use in other routes
//         req.decoded = decoded;
//         next();
//       }
//     });
//
//   } else {
//
//     // if there is no token
//     // return an error
//     return res.status(403).send({
//       success: false,
//       message: 'No token provided.'
//     });
//
//   }
// });
//
// module.exports = router;






// const checkToken = (req, res, next) => {
//   let token = req.headers['x-access-token'];
//
//   if(token) {
//     jwt.verify(token, 'secret', function (err, decoded){
//       if(err) {
//         res.json({success: false, message: 'Failed to authenticate token.'})
//       } else {
//         next();
//       }
//     })
//   } else {
//     console.log('else  ', token);
//     res.status(403).send({success: false});
//     return null;
//   }
// }
//
// module.exports = checkToken;