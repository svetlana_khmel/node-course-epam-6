const express = require('express');
const router = express.Router();
const uuidv1 = require('uuid/v1');
const getProducts = require('../controllers/products');
const parsedCookies = require('../middlewares/parsedCookies');
const parsedQuery = require('../middlewares/parsedQuery');
const checkToken = require('./checkToken');

const models = require('../server/models/index');

router.use(parsedCookies);
router.use(parsedQuery);

router.get('/',checkToken, (req, res) => {
    models.Product.findAll({}).then(function(product) {
      res.json(product);
    });
});

router.get('/:id', checkToken, (req, res) => {
  models.Product.find({
    where: {
      id: req.params.id
    }
  }).then(function(todo) {
    res.json(todo);
  });
});

// router.get('/:id/reviews', checkToken, (req, res) => {
//   models.Todo.find({
//     where: {
//       id: req.params.id
//     }
//   }).then(function(todo) {
//     res.json(todo);
//   });
//
//   res.json(getProducts('reviews', req.params.id)[0].review);
// });

// Send POST request with data:
// {
// "name": "Supreme T-Shirt 10",
//   "brand": "Supreme",
//   "price": "99.99",
//   "options": [
//   { "color": "blue" },
//   { "size": "XL" }
// ],
//   "review": "IMDb goes behind-the-scenes with movie prop master."
// }

router.post('/', checkToken,  (req, res) => {
  getProducts('post', uuidv1(), req.body);

  models.Products.create({
    id: uuidv1(),
    ...req.body
  }).then(function(product) {
    res.json(product);
  });
  res.end();
});

module.exports = router;
