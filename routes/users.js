const express = require('express');
const router = express.Router();
//const getUsers = require('../controllers/users');
const checkToken = require('./checkToken');
const models = require('../server/models/index');

router.get('/', checkToken, (req, res) => {
  //res.json(getUsers());

  models.User.findAll({}).then(function(user) {
    res.json(user);
  });
});

module.exports = router;
