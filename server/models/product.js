'use strict';
module.exports = (sequelize, DataTypes) => {
  var Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    id: DataTypes.INTEGER,
    brand: DataTypes.STRING,
    options: DataTypes.JSON,
    price: DataTypes.INTENGER,
    review: DataTypes.STRING
  }, {});
  Product.associate = function(models) {
    Product.belongsTo(models.User);
  };
  return product;
};