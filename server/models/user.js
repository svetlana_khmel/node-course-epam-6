'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    name: DataTypes.STRING,
    id: DataTypes.INTEGER
  }, {});
  User.associate = function(models) {
    User.hasMany(models.Product);
  };
  return User;
};